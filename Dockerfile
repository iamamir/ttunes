FROM openjdk:15
ADD /target/ttunes-0.0.1-SNAPSHOT.jar ttunes-tl-api.jar
ENTRYPOINT ["java","-jar","ttunes-tl-api.jar"]