package com.example.ttunes.database;

import com.example.ttunes.logger.Logger;
import com.example.ttunes.models.Country;
import com.example.ttunes.models.Customer;
import com.example.ttunes.models.Spender;
import com.example.ttunes.models.TopGenre;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * this class will handle the related CRU db operations for the customers
 */
public class CustomerRepo {
    // the connection that is used to perform each db operation
    private final DBConnection dbConnection = new DBConnection();

    private final Logger logger = new Logger();

    /**
     * to get a list of all the customer exisiting in the db
     *
     * @return list of type customers
     */
    public List<Customer> getCustomers() {
        List<Customer> customers = new ArrayList<>();
        String query = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {

                    customers.add(new Customer(
                            rs.getInt("CustomerId"),
                            rs.getString("FirstName"),
                            rs.getString("LastName"),
                            rs.getString("Country"),
                            rs.getString("PostalCode"),
                            rs.getString("Phone"),
                            rs.getString("Email")
                    ));
                }

            }
            logger.log("Select all customers successful");

        } catch (SQLException e) {
            logger.log(e.toString());
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return customers;
    }

    /**
     * to get a specific customer from the db
     *
     * @param customerId Pk used to search for the specific customer
     * @return the customer
     */
    public Customer getCustomer(int customerId) {
        Customer customer = null;
        String query = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer WHERE CustomerId = ?";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setInt(1, customerId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {

                    customer = new Customer(
                            rs.getInt("CustomerId"),
                            rs.getString("FirstName"),
                            rs.getString("LastName"),
                            rs.getString("Country"),
                            rs.getString("PostalCode"),
                            rs.getString("Phone"),
                            rs.getString("Email")
                    );
                }

            }
            logger.log("Select specific customer successful");
        } catch (SQLException e) {
            logger.log(e.toString());
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return customer;
    }

    /**
     * to check if a customer exists in the db
     * this is to be used before getting a specific customer or updating specific customer
     * it performs as a checker, to avoid the need to try to perform update or get a customer that does not exist
     *
     * @param customerId
     * @return returns 1 if the customer exist, 0 if the customer does not exist
     */
    public int isExist(int customerId) {
        int count = 0;
        String query = "SELECT count(*) FROM Customer WHERE CustomerId = ?";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setInt(1, customerId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }

            }
            logger.log("Check if specific customer exist successful");

        } catch (SQLException e) {
            logger.log(e.toString());
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return count;
    }

    /**
     * to add a new customer to the db
     *
     * @param customer the customer to be added.
     * @return true if the customer is added successfully
     */
    public boolean addNewCustomer(Customer customer) {
        boolean status = true;
        String query = "INSERT into Customer ( FirstName, LastName, Country, PostalCode, Phone, Email) values (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getCountry());
            ps.setString(4, customer.getPostalCode());
            ps.setString(5, customer.getPhoneNumber());
            ps.setString(6, customer.getEmail());

            ps.executeUpdate();
            logger.log("Add specific customer successful");


        } catch (SQLException e) {
            logger.log(e.toString());
            status = false;
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }

        return status;
    }

    /**
     * to update a customer
     *
     * @param updatedCustomer the customer to be updated, it searches for the customer,
     *                        and then updates all the attributes (excluding the PK) for that customer.
     * @return true if the customer updated successfully
     */
    public boolean updateCustomer(Customer updatedCustomer) {
        boolean status = true;
        String query = "UPDATE Customer set FirstName = ?, LastName = ?, Country=?, PostalCode =?, Phone =?,Email=? where CustomerId = ?";

        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {

            ps.setString(1, updatedCustomer.getFirstName());
            ps.setString(2, updatedCustomer.getLastName());
            ps.setString(3, updatedCustomer.getCountry());
            ps.setString(4, updatedCustomer.getPostalCode());
            ps.setString(5, updatedCustomer.getPhoneNumber());
            ps.setString(6, updatedCustomer.getEmail());
            ps.setInt(7, updatedCustomer.getCustomerId());

            ps.executeUpdate();
            logger.log("Update specific customer successful");

        } catch (SQLException e) {
            logger.log(e.toString());
            status = false;
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return status;
    }

    /**
     * @return returns the countries and the number of customers in each country in a desc order
     * it counts every customer for each country and groups it by the country
     */
    public List<Country> getCountriesAndNumberOfCustomers() {
        List<Country> countries = new ArrayList<>();
        String query = "SELECT  Country, count(*)  AS NumberOfCustomers from Customer group by Country order by NumberOfCustomers desc ;";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    countries.add(new Country(
                            rs.getString("Country"),
                            rs.getInt("NumberOfCustomers")
                    ));
                }
            }
            logger.log("Select Countries with number of customers successful");

        } catch (SQLException e) {
            logger.log(e.toString());
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return countries;
    }

    /**
     * @return a list of the most spending customers by summing the total spending for each customer,
     * then order the list in a desc order to have the most spending customers on top of the list
     */
    public List<Spender> getSpenders() {
        List<Spender> spenders = new ArrayList<>();
        String query = "SELECT  Customer.CustomerId,Customer.FirstName,Customer.LastName, SUM(Invoice.Total) AS Spending\n" +
                "                FROM Invoice\n" +
                "                         INNER JOIN Customer on Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.CustomerId ORDER BY Spending DESC;";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {

                    spenders.add(new Spender(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            Math.round(rs.getDouble(4))
                    ));
                }

            }
            logger.log("Select most spending customers successful");

        } catch (SQLException e) {
            logger.log(e.toString());
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return spenders;
    }

    /**
     * it returns a list of TopGenres for a specific customer in a desc order,
     * the first is the most popular one
     *
     * @param customerId it's used to find the genres
     * @return list of type TopGenres
     */
    public List<TopGenre> getMostPopularGenreForSpecCustomer(int customerId) {
        List<TopGenre> topGenres = new ArrayList<>();
        String query = "SELECT GenreId, Name AS Genre, Number\n" +
                "FROM (SELECT G.GenreId, G.Name, COUNT(G.Name) AS Number\n" +
                "      FROM Customer\n" +
                "               LEFT JOIN Invoice I on Customer.CustomerId = I.CustomerId\n" +
                "               LEFT JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId\n" +
                "               LEFT JOIN Track T on T.TrackId = IL.TrackId\n" +
                "               LEFT JOIN Genre G on G.GenreId = T.GenreId\n" +
                "      WHERE Customer.CustomerId = ?\n" +
                "      GROUP BY G.Name\n" +
                "      ORDER BY Number DESC)\n" +
                "WHERE Number = (SELECT MAX(Number)\n" +
                "                FROM (SELECT  G.Name, COUNT(G.Name) AS Number\n" +
                "                      FROM Customer\n" +
                "                               LEFT JOIN Invoice I on Customer.CustomerId = I.CustomerId\n" +
                "                               LEFT JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId\n" +
                "                               LEFT JOIN Track T on T.TrackId = IL.TrackId\n" +
                "                               LEFT JOIN Genre G on G.GenreId = T.GenreId\n" +
                "                      WHERE Customer.CustomerId = ?\n" +
                "                      GROUP BY G.Name\n" +
                "                      ORDER BY Number DESC))";
        try (PreparedStatement ps = dbConnection.conn().prepareStatement(query)) {
            ps.setInt(1, customerId);
            ps.setInt(2, customerId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    topGenres.add(
                            new TopGenre(
                                    rs.getInt(1),
                                    rs.getString(2),
                                    rs.getInt(3)
                            )
                    );
                }
            }

            logger.log("Select top generes successful");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                dbConnection.conn().close();
            } catch (SQLException e) {
                logger.log(e.toString());
            }
        }
        return topGenres;
    }

}
