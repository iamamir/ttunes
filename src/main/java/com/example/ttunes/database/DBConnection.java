package com.example.ttunes.database;

import com.example.ttunes.logger.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * this class will handle establishing connection to the db
 */
public class DBConnection {

    /**
     * to open the connection
     * @return an open connection to the db
     */
    public Connection conn(){

       String url = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
        try {
            new Logger().log("Connection to SQLite has been established.");
            return  DriverManager.getConnection(url);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
}
