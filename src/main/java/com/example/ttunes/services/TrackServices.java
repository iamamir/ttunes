package com.example.ttunes.services;

import com.example.ttunes.database.TrackRepo;
import com.example.ttunes.models.Track;

import java.util.List;
import java.util.Objects;

/**
 * this class will serve as a middle man between Track repo and both home and search controllers,
 * to provide less written code in the controller and eliminate errors and readability reasons
 */
public class TrackServices {

    private final TrackRepo trackRepo = new TrackRepo();

    /**
     *
     * @return list of 5 random tracks if the list is no
     */
    public List<Track> getRandTracks() {
        List<Track> tracks = trackRepo.getRandTracks();
        if (tracks.size() > 0)
            return tracks;
        return null;
    }

    public List<Track> getSearchedTracks(String name) {
        List<Track> tracks = null;
        if (Objects.requireNonNull(name).length() != 0) {
            tracks = trackRepo.getSearchedTrack(name);
        }
        assert tracks != null;
        if (tracks.size() > 0)
            return tracks;
        return null;
    }
}
