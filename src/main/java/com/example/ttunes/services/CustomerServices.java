package com.example.ttunes.services;

import com.example.ttunes.database.CustomerRepo;
import com.example.ttunes.models.Country;
import com.example.ttunes.models.Customer;
import com.example.ttunes.models.Spender;
import com.example.ttunes.models.TopGenre;

import java.util.List;

/**
 * this class will serve as a middle man between customer repo and the customer controller,
 * to provide less written code in the controller and eliminate errors and readability reasons
 */
public class CustomerServices {
    // instance of the customer repo to use its methods
    private final CustomerRepo customerRepo = new CustomerRepo();


    /**
     * it checks if the list is empty or not,
     *
     * @return returns the list if it has customers
     */
    public List<Customer> getCustomers() {
        List<Customer> customers = customerRepo.getCustomers();
        if (customers.size() > 0)
            return customers;
        return null;
    }

    /**
     * to get a customer, it checks first if the customer exits
     * in case it exists, a customer is returned
     *
     * @param customerId
     * @return
     */
    public Customer getCustomer(int customerId) {
        if (customerRepo.isExist(customerId) > 0) {
            return customerRepo.getCustomer(customerId);
        }
        return null;
    }

    /**
     * to add a customer, checks if if got an empty customer object
     * it returns true if the customer is added successfully
     *
     * @param customer
     * @return
     */
    public boolean addCustomer(Customer customer) {
        if (customer != null) {
            return customerRepo.addNewCustomer(customer);
        }
        return false;
    }

    /**
     * to update a customer, checks first if received a customer to update
     * then if the cusomer exists in the db, if both are true, it updates the customer and returns true
     *
     * @param customer
     * @return
     */
    public boolean updateCustomer(Customer customer) {
        if (customer != null) {
            if (customerRepo.isExist(customer.getCustomerId()) > 0)
                return customerRepo.updateCustomer(customer);
        }
        return false;
    }

    /**
     * checks if the list of countries is not empty, if yes it returns
     * a list of the countries and the number of customers
     *
     * @return
     */
    public List<Country> getNumberOfCustomerInCountries() {
        List<Country> countries = customerRepo.getCountriesAndNumberOfCustomers();
        if (countries.size() != 0)
            return countries;
        return null;
    }

    /**
     * checks if the ist of spenders is not empty, if yes, it returns
     * a list of spenders
     *
     * @return
     */
    public List<Spender> getSpendingCustomers() {
        List<Spender> spendingCustomers = customerRepo.getSpenders();
        if (spendingCustomers.size() != 0)
            return spendingCustomers;
        return null;
    }

    /**
     * to get the list of top genres for a specific user
     * @param cusomterId it takes in the customerId to check if the customer exists, if the customer exists, it then looks for the top genres for this customer
     * @return it returns a list of the top genres, it can be one or more, if they have the same number
     */
    public List<TopGenre> getMostPopularGenre(int cusomterId) {
        List<TopGenre> topGenres = null;
        if (customerRepo.isExist(cusomterId) > 0) {
            topGenres = customerRepo.getMostPopularGenreForSpecCustomer(cusomterId);
            if (topGenres.size() != 0)
                return topGenres;
        }
        return topGenres;
    }


}
