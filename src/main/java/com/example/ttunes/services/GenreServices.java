package com.example.ttunes.services;

import com.example.ttunes.database.GenreRepo;
import com.example.ttunes.models.TopGenre;
import java.util.List;

/**
 * this class will serve as a middle man between Genre repo and home controller,
 * to provide less written code in the controller and eliminate errors and readability reasons
 */
public class GenreServices {

    private final GenreRepo genreRepo = new GenreRepo();

    /**
     *
     * @return list of 5 random genres if the list is not empty
     */
    public List<TopGenre> getRandGenres() {
        List<TopGenre> genres = genreRepo.getRandGenres();
        if (genres.size() > 0)
            return genres;
        return null;
    }
}
