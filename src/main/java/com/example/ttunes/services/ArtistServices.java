package com.example.ttunes.services;

import com.example.ttunes.database.ArtistRepo;
import com.example.ttunes.models.Artist;
import java.util.List;

/**
 * this class will serve as a middle man between Artist repo and home controller,
 * to provide less written code in the controller and eliminate errors and readability reasons
 */
public class ArtistServices {

    private final ArtistRepo artistRepo = new ArtistRepo();

    /**
     *
     * @return list of 5 random artists if the list is not empty
     */
    public List<Artist> getRandArtists() {
        List<Artist> artists = artistRepo.getRandArtists();
        if (artists.size() > 0)
            return artists;
        return null;
    }


}
