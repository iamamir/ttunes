package com.example.ttunes.controllers.rest;

import com.example.ttunes.models.Country;
import com.example.ttunes.models.Customer;
import com.example.ttunes.models.Spender;
import com.example.ttunes.models.TopGenre;
import com.example.ttunes.services.CustomerServices;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * rest controller that handles the customer requests
 */
@RestController
// setting the root path
@RequestMapping(value = "api/customers", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {
    private CustomerServices cs = new CustomerServices();

    /**
     *
     * @return get request that returns a list of customers
     */
    @GetMapping
    public List<Customer> getCustomers() {
        return cs.getCustomers();
    }

    /**
     * post request to add a customer, it takes a customer from the body
     * @param customer
     * @return String message added, if it is successfully added, or try again if not successful
     */
    @PostMapping
    public String addCustomer(@RequestBody() Customer customer) {
        if (cs.addCustomer(customer))
            return "Added";
        else
            return "Please try again";
    }

    /**
     * get request to get a specific customer
     * @param customerId
     * @return returns a specific customer, if the given id has a customer linked to it
     */
    @GetMapping("/{customerId}")
    public Customer getCustomer(@PathVariable int customerId) {
        return new CustomerServices().getCustomer(customerId);
    }

    /**
     * put request to update a specific customer
     * @param customerId it's just safer to use the same id from the Path, to avoid if the user enters diff id that is not his which may result in updating diff user data
     * @param customer customer to be updated
     * @return updated if successfully or try again, in case the user id does not exist
     */
    @PutMapping("/{customerId}")
    public String updateCustomer(@PathVariable int customerId, @RequestBody() Customer customer) {
        customer.setCustomerId(customerId);
        if (cs.updateCustomer(customer))
            return "Updated";
        return "Please try again";

    }

    /**
     * Get request that returns the top genres for a specific user
     * @param customerId
     * @return
     */
    @GetMapping("/{customerId}/genre")
    public List<TopGenre> getTopGenre(@PathVariable int customerId) {
        return cs.getMostPopularGenre(customerId);

    }

    /**
     * get request to get the list of spenders, it returns the full list, the top are the once who spend the most, and the bottom are the least spenders
     * @return
     */
    @GetMapping("/spenders")
    public List<Spender> getSpenders() {
        return cs.getSpendingCustomers();
    }

    /**
     * get request to get a list of countries and the number of the customers in each country,
     * on the top the countries with the most customers and the bottom the countries with least customers
     * @return
     */
    @GetMapping("/countries")
    public List<Country> getCustomersInCountries() {
        return cs.getNumberOfCustomerInCountries();
    }
}
