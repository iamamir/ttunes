package com.example.ttunes.controllers.thymeleaf;

import com.example.ttunes.services.ArtistServices;
import com.example.ttunes.services.GenreServices;
import com.example.ttunes.services.TrackServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * thymeleaf controller to handle the home page controller, where it shows three diff tables, as well as a search field
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    /**
     * a get method that returns a map models that has three different objects each containing a list.
     * and it renders the index (homepage)
     * @param model
     * @return
     */
    @GetMapping
    public String getRandoms(ModelMap model) {
        model.put("artists", new ArtistServices().getRandArtists());
        model.put("genres", new GenreServices().getRandGenres());
        model.put("tracks", new TrackServices().getRandTracks());
        return "index";
    }
}
