package com.example.ttunes.controllers.thymeleaf;

import com.example.ttunes.services.TrackServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * thymeleaf controller to handle the requests of the search page
 */
@Controller
@RequestMapping("/search")
public class SearchController {

    /**
     * a get method that takes the search word as parameter and a model. and then it returns a list of found results (tracks) in a model object
     * as well as the search keyword. and renders the search page.
     * @param track
     * @param model
     * @return
     */
    @GetMapping
    public String getSearch(@RequestParam(defaultValue = "") String track, ModelMap model) {
        model.put("searchResult", "Search result for: " + track);
        model.put("tracks", new TrackServices().getSearchedTracks(track));
        return "search";
    }

}
