package com.example.ttunes.models;

/**
 * country model class, used to contain the attributes when searching for number of customers in each country
 */
public class Country {
    private String countryName;
    private int numberOfCustomers;

    public Country() {
    }

    public Country(String countryName, int numberOfCustomers) {
        this.countryName = countryName;
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }
}
