# TTunes application 
## TTunes is a spring-maven application that supports both thymeleaf and a rest api. In addition, the application consumes SQLite as the main database. Futhermore the application is deployed and successfully running on both heroku and docker.
### This application has two instances on heroku, one of which it was pushed directly to heroku, and the other, first  it was created an image of it on docker, and then deployed the docker image to heroku.
### [TTunes on heroku](https://the-ttunes.herokuapp.com/home)
### [TTunes docker image on heroku](https://ttunes-tl-api.herokuapp.com/home)
### [Docker repo](https://hub.docker.com/repository/docker/amirsg/ttunestlapi)
### ![Both apps on heroku](https://gitlab.com/iamamir/ttunes/-/raw/master/src/main/resources/static/img/herokuappsPNG.PNG)

---
### The Thymeleaf operations: 
##### 1. Fetch and display 5 randoms artists, tracks and genres from the database.
##### 2. Search for tracks by name.

### The RESTAPI operations: 
##### 1. Get all customers that exist in the database.
##### 2. Get a specific customer that exist in the database.
##### 3. Update a specific customer that exist in the database
##### 4. Get all the countries and the number of customer in each country, the list is ordered according to the number of customers in each country. Countries with a lot of customers after on top of the list
##### 5. Get all the spending customers and the amount they spent, the list is ordered according to the amount spent. Top spenders are on top of the list
##### 6. Get the most popular genre for a specific customer.
##### 7. Add a new customer.
#### Here's a link to [the postman collections](https://gitlab.com/iamamir/ttunes/-/tree/master/src/main/resources/PostManCollections), where all the requests for the RESTAPI operations are saved.
### Futhermore, the application logs every executed operation and print it to the console.
---
## Error handling have been implemented to prevent errors from crashing the application while run time.